const path = require('path')

module.exports = function completeRepos (input, pathSoFar = '') {
  if (typeof input === 'string') {
    return {
      path: path.join(pathSoFar, input.split('/').pop()),
      src: toGitRepo(input),
      nested: []
    }
  }

  pathSoFar = input.dir
    ? path.join(pathSoFar, input.dir)
    : path.join(pathSoFar, nameFromRepo(input.repo))

  const output = {
    path: pathSoFar,
    src: toGitRepo(input.repo),
    nested: []
  }

  if (input.nested) {
    for (const _input of input.nested) {
      output.nested.push(completeRepos(_input, pathSoFar))
    }
  }

  return output
}

function toGitRepo (location) {
  if (!location) return
  if (location.endsWith('.git')) return location

  return `git@gitlab.com:${location}.git`
}

function nameFromRepo (repo) {
  return repo.replace('.git', '').split('/').pop()
}
