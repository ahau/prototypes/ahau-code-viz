const path = require('path')
const mkdirp = require('mkdirp')
const { spawn, exec } = require('child_process')
const pull = require('pull-stream')
const parallelMap = require('pull-paramap')

const complete = require('./complete-repos')
const repos = require('./repos')

processRepo(complete(repos), (err) => {
  if (err) throw err

  totalLog((err) => {
    if (err) return print(err.message, true)

    print('build TOTAL.txt', null)
  })
})

/* util functions */

function processRepo (repo, cb) {
  clone(repo, (err, repo) => {
    if (err) return cb(err)

    extractLog(repo, (err, fileName) => {
      const msg = repo.src && repo.src + new Array(120 - repo.src.length).fill(' ').join('')
      if (err) {
        msg && print(msg, true) // show red ✗
        return cb(err)
      }

      msg && print(msg, null) // show green ✓

      pull(
        pull.values(repo.nested),
        parallelMap(processRepo, 10),
        pull.collect(cb)
      )
    })
  })
}

const RECEIVING = Buffer.from('Receiving objects')
const RESOLVING = Buffer.from('Resolving deltas')
function clone (repo, cb) {
  const dir = path.join(__dirname, ...repo.path.split('/').slice(0, -1))
  mkdirp.sync(dir)

  if (!repo.src) return cb(null, repo)

  let isDone = false
  const clone = spawn('git', ['clone', repo.src, repo.path, '--progress'])

  clone.stderr.on('data', buf => {
    if (isDone) return

    if (
      !buf.slice(0, RECEIVING.length).equals(RECEIVING) &&
      !buf.slice(0, RESOLVING.length).equals(RESOLVING)
    ) return

    print(`${repo.src}  ${purple(buf.toString('utf8').replace('\n', ''))}`)
  })

  clone.on('close', code => {
    isDone = true
    code === 0
      ? cb(null, repo)
      : cb(new Error('error while cloning, aborting'), repo)
  })
}

function extractLog (repo, cb) {
  if (!repo.src) return cb(null)

  const fileName = `${repo.path.split('/').pop()}.txt`

  exec(`gource --output-custom-log build/${fileName} ${repo.path}`, (err) => {
    if (err) return handleError(err, `unable to extractLog for ${repo.path}`)

    const attachPath = repo.path.replace('build/', '')

    shiftLog(fileName, attachPath, (err) => {
      if (err) return handleError(err, `unable to run shiftLog on ${fileName}`)

      cb(null, fileName)
    })
  })
  function handleError (err, msg) {
    console.error(msg)
    cb(err)
  }
}

function shiftLog (fileName, attachPath, cb) {
  attachPath = attachPath.replace(/\//g, '\\/')

  exec(
    `sed -i -r 's/([^\\|]+)$/\\/${attachPath}\\1/' build/${fileName}`,
    (err, stderr, stdout) => {
      if (err) return cb(err)

      cb(null, fileName)
    }
  )
}

function totalLog (cb) {
  // cat log1.txt log2.txt | sort -n > TOTAL.txt
  // cat *.txt | sort -n > TOTAL.txt

  exec('cat build/*.txt | sort -n > build/TOTAL.txt', (err) => {
    if (err) return cb(err)

    exec(`sed -i -e "
      s/andr[^|]+staltz/staltz/I;
      s/ben[^|]*/ben/I;
      s/blake[^|]*/blake/I;
      s/cherese[^|]*/cherese/I;
      s/christian[^|]*/christian/I;
      s/colin[^|]*/colin/I;
      s/ian[^|]*/ian/I;
      s/luandro[^|]*/luandro/I;
      s/mix[^|]*/mix/I;
      s/r.mulo[^|]*/romulo/I;
    " build/TOTAL.txt`, cb)
  })
}

function print (msg, err) {
  let symbol = ' '
  if (err) symbol = red('✗')
  if (err === null) symbol = green('✓')

  const newLine = (err !== undefined) ? '\n' : ''

  process.stdout.write(`\r ${symbol} ${msg}${newLine}`)
}

function green (str) { return `\x1b[32m${str}\x1b[0m` }
function red (str) { return `\x1b[31m${str}\x1b[0m` }
function purple (str) { return `\x1b[35m${str}\x1b[0m` }
